package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.lang.Math;
import java.util.Collections;
import java.util.ListIterator;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minimum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {



        int size = getPyramidSize(inputNumbers.size());
        int floor = 2 * size - 1;
        int [][] pyramid = new int [size][floor];

        try {
            Collections.sort(inputNumbers);}
        catch(NullPointerException e){
            throw new CannotBuildPyramidException();
        }
            ListIterator<Integer> iter = inputNumbers.listIterator();

            for (int i = 0; i < size; i++) {
                int insertIndex = size - (i + 1);
                for (int j = 0; j <= i; j++) {
                    pyramid[i][insertIndex] = iter.next();
                    insertIndex += 2;
                }
            }

            return pyramid;

    }

    /**
     * Calculate the pyramid size (not including zero-spacing).
     * Calculation is based on the formula for the sum of n natural numbers:
     *  S = n(n+1)/2 -> n^2 + n -2S = 0 -> n = (sqrt(1 + 8S) - 1) / 2
     *
     * @param listSize the initial size of the list for calculation
     * @return an int size of the pyramid
     * @throws {@link CannotBuildPyramidException} if (1 + 8S) is not a triangular number
     */
    private int getPyramidSize(int listSize){

        int discriminant = 1 + 8*listSize;

        if(isSquareNumber(discriminant)){
            int n = (int) (Math.sqrt(discriminant) - 1)/2;
            return n;
        }
        else {
            throw new CannotBuildPyramidException();
        }
    }

    /**
     * Check if the given number is a square of other natural number.
     *
     * @param number the number to be checked
     * @return boolean True if the number is square and False if it isn't
     */
    private boolean isSquareNumber(int number){
        double root = Math.sqrt(number);
        return ((root - Math.floor(root)) == 0);
    }


}
