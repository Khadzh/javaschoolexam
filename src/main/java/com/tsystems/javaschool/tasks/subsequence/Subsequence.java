package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if(y == null || x == null)   throw new IllegalArgumentException();

        int x_index = 0, y_index = 0;
        while (x_index < x.size()) {
                if (y_index == y.size()) {
                    return false;
                }
                if (x.get(x_index).equals(y.get(y_index))) {
                    x_index++;
                }
                y_index++;
        }
        return true;

    }
}
