package com.tsystems.javaschool.tasks.calculator;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Stack;

public class Calculator {

    private final static String HIGHER_OPERATORS = "*/";
    private final static String LOWER_OPERATORS = "+-";
    private final static String OPERATORS = HIGHER_OPERATORS+LOWER_OPERATORS;

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        if(statement == null || statement.isEmpty()){
            return null;
        }

        try{
            List<String> listStatement = parseStatementToList(statement);
            String result = String.format(Locale.US,"%.4f", evaluate(listStatement))
                    .replaceAll("((\\.[123456789]+)(0+$))|(\\.0+$)","$2");
            return result;
        } catch( EvaluationGeneralException e){
            return null;
        }

    }

    /**
     * Parse string to list of chunks of allowed types (parenthesis, numbers and operators).
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return list of String chunks from the initial String statement
     * @throws {@link EvaluationGeneralException} if the statement contains forbidden characters
     * and therefore cannot be parsed
     */
    List<String> parseStatementToList(String statement) throws EvaluationGeneralException {
        statement = statement.replaceAll("\\s+", "");
        List<String> listStatement = new ArrayList<>();
        String curChar;
        boolean openParenthesis = false;

        for(int i = 0; i < statement.length(); i++){
            curChar = statement.substring(i,i+1);
            if(curChar.equals("(")){
                if(openParenthesis) throw new EvaluationGeneralException();
                openParenthesis = true;
                listStatement.add(curChar);
            }
            else if(curChar.equals(")")){
                if(openParenthesis) {
                        listStatement.add(curChar);
                        openParenthesis = false;
                    }
                else throw new EvaluationGeneralException();
                }
            else if(curChar.matches("\\d")){
                i = parseNumber(statement,i,listStatement);
            }
            else if(OPERATORS.contains(curChar)){
                if(listStatement.isEmpty()) throw new EvaluationGeneralException();
                String prevElement = listStatement.get(listStatement.size() - 1);
                if (!OPERATORS.contains(prevElement)) listStatement.add(curChar);
                else throw new EvaluationGeneralException();
            }
            else{
                throw new EvaluationGeneralException();
            }
        }

        if(openParenthesis || OPERATORS.contains(listStatement.get(listStatement.size() - 1))){
            throw new EvaluationGeneralException();
        }

        return listStatement;
    }

    /**
     * Parse one number (digits and . as decimal mark) from the statement string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @param startIndex int index indicating parsed number starting point
     * @param result List of statement chunks from the calling function, the parsed number is added to it.
     * @return returns the ending index of parsed number for further parsing from the caller function.
     * @throws {@link EvaluationGeneralException} if the statement cannot be parsed
     */
    private int parseNumber(String statement, int startIndex, List<String> result){
        String curChar;
        boolean hasDot = false;
        String number = statement.substring(startIndex, startIndex + 1);
        for(int i = startIndex+1; i < statement.length(); i++){
            curChar = statement.substring(i, i + 1);
            if (curChar.matches("\\d") || (curChar.equals(".") && !hasDot)){
                number += curChar;
                if (curChar.equals(".")) hasDot = true;
            }
            else break;
        }
        result.add(number);
        return startIndex + number.length() - 1;
    }

    /**
     * Go through the stack of operators until the stack
     * is empty or an opening parenthesis is reached.
     * Pop left ang right operands from the numbers stack and push the calculation result back on stack.
     * NB The ranking order of operators is observed in the calling function.
     *
     * @param statement List of parsed statement chunks.
     * @throws {@link EvaluationGeneralException} in case of division by zero (propagated from calculateStack()).
     */
    private double evaluate(List<String> statement){
        Stack<Double> numbers = new Stack<>();
        Stack<String> operators = new Stack<>();

        for(String c : statement){
            if (c.matches("\\d+(\\.\\d+)?")){
                numbers.push(Double.parseDouble(c));
            } else if(OPERATORS.contains(c)){
                if (!operators.empty() && hasGraterOrder(operators.peek(),c)) {
                    calculateStack(numbers, operators);
                }
                operators.push(c);
            }else if(c.equals("(")){
                operators.push(c);
            }
            else if(c.equals(")")){
                calculateStack(numbers,operators);
            }
        }
        calculateStack(numbers,operators);
        return numbers.pop();
    }

    /**
     * Go through the stack of operators until the stack
     * is empty or an opening parenthesis is reached.
     * Pop left ang right operands from the numbers stack and push the calculation result back on stack.
     * NB The ranking order of operators is observed in the calling function.
     *
     * @param numbers all previous values from the statement in the LIFO order.
     * @param operators all the preceding operators with the same or greater order.
     * @throws {@link EvaluationGeneralException} in case of division by zero.
     */
    private void calculateStack(Stack<Double> numbers, Stack<String> operators){
        while(!operators.empty()){
            String operator = operators.pop();
            if(operator.equals("(")){
                return;
            }
            double right = numbers.pop();
            double left = numbers.pop();
            switch(operator){
                case "+" :
                    numbers.push(left+right);
                    break;
                case "-" :
                    numbers.push(left-right);
                    break;
                case "*" :
                    numbers.push(left*right);
                    break;
                case "/" :
                    if(right == 0)
                        throw new EvaluationGeneralException();
                    numbers.push(left/right);
                    break;
            }
        }
    }

    private boolean hasGraterOrder(String first, String second){
        if(HIGHER_OPERATORS.contains(first)||
                (LOWER_OPERATORS.contains(first) && LOWER_OPERATORS.contains(second))){
            return true;
        }
        return false;
    }

}
